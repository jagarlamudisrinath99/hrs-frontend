/* eslint-disable no-undef */
import axios from 'axios';

const client = axios.create({ baseURL: 'http://hrs.example.local/api' });

export default {
  get: client.get,
  post: client.post,
  put: client.put,
  delete: client.delete,
};
