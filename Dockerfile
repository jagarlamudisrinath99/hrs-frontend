# pull the latest official nginx image
FROM nginx:stable
# run docker service on HTTPS
EXPOSE 80
# copy static maintanence
COPY ./build/ /usr/share/nginx/html/
STOPSIGNAL SIGQUIT
CMD ["nginx", "-g", "daemon off;"]